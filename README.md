# Slurm cluster on docker

Version based on https://github.com/giovtorres/slurm-docker-cluster

Instead of Centos, this one is based on ubuntu.



Usage example:

- In one terminal start the processes and follow log:
```
docker-compose up --build -d
docker-compose logs -f
```

- In a second terminal enter controller, and start jobs:
```
docker-compose exec slurmctld bash
scontrol  show nodes
srun -c 1 /usr/bin/time stress -t 5 -c 2
srun --mem 512M /usr/bin/time stress -t 5 --vm 1 --vm-bytes 2G
```



