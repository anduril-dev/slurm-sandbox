FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
     munge \
     gosu \
     mariadb-client \
  && apt-get clean

RUN apt-get update && apt-get install -y \
     slurmctld \
     slurm-client \
  && apt-get clean

RUN mkdir -p \
        /var/spool/slurmd \
        /var/run/munge \
        /var/run/slurmd \
        /var/run/slurmdbd \
        /var/lib/slurmd \
        /var/log/slurm-llnl \
        /data \
    && touch /var/lib/slurmd/node_state \
        /var/lib/slurmd/front_end_state \
        /var/lib/slurmd/job_state \
        /var/lib/slurmd/resv_state \
        /var/lib/slurmd/trigger_state \
        /var/lib/slurmd/assoc_mgr_state \
        /var/lib/slurmd/assoc_usage \
        /var/lib/slurmd/qos_usage \
        /var/lib/slurmd/fed_mgr_state \
    && chown -R slurm:slurm /var/*/slurm* \
    && chown -R munge:munge /var/*/munge*

COPY cgroup.conf /etc/slurm-llnl/cgroup.conf
COPY slurm.conf /etc/slurm-llnl/slurm.conf
COPY slurmdbd.conf /etc/slurm-llnl/slurmdbd.conf
ADD entry.sh /

ENTRYPOINT ["/entry.sh"]
